#ifndef IPC_SERVER_H
#define IPC_SERVER_H

#include <unistd.h>
#include <inttypes.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>

#include "ipc_types.h"

int
server_send_message(int socket, uint8_t *ipc_message, uint32_t len);
int
server_send_swapchain_with_fds(int socket,
                               ipc_swapchain_message_t *ipc_swapchain_message);

#endif
