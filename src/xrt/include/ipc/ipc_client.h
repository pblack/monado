#ifndef IPC_CLIENT_H
#define IPC_CLIENT_H

#include <unistd.h>
#include <inttypes.h>
#include <sys/types.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>

#include "ipc_types.h"
#include "xrt/xrt_compositor.h"

bool
ipc_connect(ipc_connection_t *ipc_c);
void
ipc_disconnect(ipc_connection_t *ipc_c);

// generic ipc message request/response call
ipc_result_t
ipc_send_message(ipc_connection_t *ipc_c, uint8_t *message, uint32_t size);

// special-case for swapchain - requires fds to be passed via CMSG structures
ipc_result_t
ipc_acquire_swapchain_fds(ipc_connection_t *ipc_c,
                          ipc_swapchain_message_t *m,
                          int *out_fds);

// compositor functions


void
ipc_compositor_begin_session(struct xrt_compositor *xc,
                             enum xrt_view_type view_type);

void
ipc_compositor_end_session(struct xrt_compositor *xc);

void
ipc_compositor_wait_frame(struct xrt_compositor *xc,
                          uint64_t *predicted_display_time,
                          uint64_t *predicted_display_period);

void
ipc_compositor_begin_frame(struct xrt_compositor *xc);

void
ipc_compositor_end_frame(struct xrt_compositor *xc,
                         enum xrt_blend_mode blend_mode,
                         struct xrt_swapchain **xscs,
                         const uint32_t *image_index,
                         uint32_t *layers,
                         uint32_t num_swapchains);


void
ipc_compositor_discard_frame(struct xrt_compositor *xc);

void
ipc_compositor_destroy(struct xrt_compositor *xc);

struct xrt_swapchain *
ipc_compositor_swapchain_create(struct xrt_compositor *xc,
                                enum xrt_swapchain_create_flags create,
                                enum xrt_swapchain_usage_bits bits,
                                uint32_t num_images,
                                int64_t format,
                                uint32_t sample_count,
                                uint32_t width,
                                uint32_t height,
                                uint32_t face_count,
                                uint32_t array_size,
                                uint32_t mip_count);

void
ipc_compositor_swapchain_destroy(struct xrt_compositor *xc);

bool
ipc_compositor_swapchain_wait_image(struct xrt_swapchain *xsc,
                                    uint64_t timeout,
                                    uint32_t index);
bool
ipc_compositor_swapchain_acquire_image(struct xrt_swapchain *xsc,
                                       uint32_t *index);
bool
ipc_compositor_swapchain_release_image(struct xrt_swapchain *xsc,
                                       uint32_t index);



/*!
 * Spew level logging.
 *
 */
#define IPC_SPEW(c, ...)                                                       \
	{                                                                      \
	}

/*!
 * Debug level logging.
 *
 */
#define IPC_DEBUG(c, ...)                                                      \
	{                                                                      \
	}


#endif
