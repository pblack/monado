#ifndef IPC_TYPES_H
#define IPC_TYPES_H

#include <inttypes.h>
#include "xrt/xrt_compiler.h"

#define MSG_SOCK_FILE "/tmp/monado_comp_ipc"
#define MAX_SWAPCHAIN_FDS 8
#define IPC_CRED_SIZE 1  // auth not implemented
#define IPC_BUF_SIZE 256 // must be >= largest message length in bytes

typedef struct ipc_connection
{
	int socket_fd;
	bool socket_created;
	bool socket_connected;

	bool print_debug; // TODO: link to settings
	bool print_spew;  // TODO: link to settings

} ipc_connection_t;

typedef struct ipc_fd_swapchain
{
	uint32_t swapchain_handle; // refers to a per-client resource
	uint32_t width;            // needed for client-side resource-wrapping
	uint32_t height;           // needed for client-side resource-wrapping
	uint64_t format;           // needed for client-side resource-wrapping
	uint64_t size;             // needed for client-side resource-wrapping
	int swapchain_fds[MAX_SWAPCHAIN_FDS]; // this is the array of fds that
	                                      // will get passed via ipc
	uint32_t num_fds;                     // number of fds passed
} ipc_fd_swapchain_t;

typedef struct ipc_swapchain_image
{
	uint32_t swapchain_handle; // refers to a per-client resource
	uint32_t index;            // index of image
	uint64_t timeout;
} ipc_swapchain_image_t;

typedef struct ipc_compositor_frame
{
	uint64_t predicted_display_time;
	uint64_t predicted_display_period;
	uint32_t blend_mode;
} ipc_compositor_frame_t;

typedef struct ipc_session
{
	uint32_t session_handle;
} ipc_session_t;

typedef struct ipc_auth
{
	uint32_t client_id;
	uint8_t client_cred[IPC_CRED_SIZE];
} ipc_auth_t;

typedef enum ipc_result
{
	IPC_SUCCESS = 0,
	IPC_FAILURE,
} ipc_result_t;

typedef enum ipc_command
{
	IPC_ERR = 0,
	IPC_AUTH,
	IPC_SWAPCHAIN_CREATE,
	IPC_SWAPCHAIN_ACQUIRE_FDS,
	IPC_SWAPCHAIN_DESTROY,
	IPC_SWAPCHAIN_ACQUIRE_IMAGE,
	IPC_SWAPCHAIN_WAIT_IMAGE,
	IPC_SWAPCHAIN_RELEASE_IMAGE,
	IPC_BEGIN_SESSION,
	IPC_END_SESSION,
	IPC_COMPOSITOR_WAIT_FRAME,
	IPC_COMPOSITOR_BEGIN_FRAME,
	IPC_COMPOSITOR_END_FRAME,
	IPC_COMPOSITOR_DISCARD_FRAME
} ipc_command_t;

typedef struct ipc_auth_message
{
	ipc_command_t command;
	int32_t result;
	ipc_auth_t auth_data;
} ipc_auth_message_t;

typedef struct ipc_swapchain_message
{
	ipc_command_t command;
	int32_t result;
	ipc_fd_swapchain_t fd_swapchain;
} ipc_swapchain_message_t;

typedef struct ipc_session_message
{
	ipc_command_t command;
	int32_t result;
	ipc_session_t session;
} ipc_session_message_t;

typedef struct ipc_swapchain_image_message
{
	ipc_command_t command;
	int32_t result;
	ipc_swapchain_image_t swapchain_image;
} ipc_swapchain_image_message_t;

typedef struct ipc_compositor_frame_message
{
	ipc_command_t command;
	int32_t result;
	ipc_compositor_frame_t compositor_frame;
} ipc_compositor_frame_message_t;


#endif
