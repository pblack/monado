﻿#include "comp_compositor_standalone.h"
#include "comp_compositor.h"
#include "comp_renderer.h"

static void *
ipc_thread_main(void *ipc_client_resource);

int
main(int argc, char *argv[])
{
	printf("STANDALONE COMPOSITOR STARTING UP!\n\n");
	struct xrt_matrix_2x2 identity_rot = {{{1.0f, 0.0f, 0.0f, 1.0f}}};
	struct xrt_hmd_parts fake_hmd;
	struct xrt_view l_eye;
	struct xrt_view r_eye;

	l_eye.fov.angle_up = 0.005f;
	l_eye.fov.angle_down = 0.005f;
	l_eye.fov.angle_left = 0.005f;
	l_eye.fov.angle_right = 0.005f;
	l_eye.rot = identity_rot;
	l_eye.display.h_meters = 1.0f;
	l_eye.display.w_meters = 1.0f;
	l_eye.display.h_pixels = 1024;
	l_eye.display.w_pixels = 1024;
	l_eye.viewport.h_pixels = 1024;
	l_eye.viewport.w_pixels = 1024;
	l_eye.viewport.x_pixels = 0;
	l_eye.viewport.y_pixels = 0;
	l_eye.lens_center.x_meters = 0.5f;
	l_eye.lens_center.y_meters = 0.5f;

	r_eye.fov.angle_up = 0.5;
	r_eye.fov.angle_down = 0.5;
	r_eye.fov.angle_left = 0.5;
	r_eye.fov.angle_right = 0.5;
	r_eye.rot = identity_rot;
	r_eye.display.h_meters = 1.0f;
	r_eye.display.w_meters = 1.0f;
	r_eye.display.h_pixels = 1024;
	r_eye.display.w_pixels = 1024;
	r_eye.viewport.h_pixels = 1024;
	r_eye.viewport.w_pixels = 1024;
	r_eye.viewport.x_pixels = 1024;
	r_eye.viewport.y_pixels = 0;
	r_eye.lens_center.x_meters = 0.5f;
	r_eye.lens_center.y_meters = 0.5f;


	fake_hmd.views[0] = l_eye;
	fake_hmd.views[1] = r_eye;
	fake_hmd.distortion.preferred = XRT_DISTORTION_MODEL_NONE;
	fake_hmd.distortion.models = XRT_DISTORTION_MODEL_NONE;
	fake_hmd.screens[0].h_pixels = 1024;
	fake_hmd.screens[0].w_pixels = 2048;
	fake_hmd.screens[0].nominal_frame_interval_ns = 16000;

	fake_hmd.distortion.mesh.vertices = NULL;
	fake_hmd.distortion.mesh.indices = NULL;
	fake_hmd.distortion.pano.warp_scale = 1.0f;
	fake_hmd.distortion.pano.aberration_k[0] = 1.0f;
	fake_hmd.distortion.pano.aberration_k[1] = 1.0f;
	fake_hmd.distortion.pano.aberration_k[2] = 1.0f;
	fake_hmd.distortion.pano.aberration_k[3] = 1.0f;

	fake_hmd.distortion.pano.distortion_k[0] = 0.0f;
	fake_hmd.distortion.pano.distortion_k[1] = 0.0f;
	fake_hmd.distortion.pano.distortion_k[2] = 0.0f;
	fake_hmd.distortion.pano.distortion_k[3] = 1.0f;

	struct xrt_device xd;
	xd.hmd = &fake_hmd;
	xd.num_inputs = 0;
	xd.num_outputs = 0;

	struct xrt_compositor *xc = xrt_gfx_provider_create_fd(&xd, false);
	struct comp_compositor *c = comp_compositor(xc);
	ipc_client_state_t client_states[IPC_MAX_CLIENTS];

	// make sure all our client connections have a handle to the compositor
	// and consistent initial state
	for (uint32_t i = 0; i < IPC_MAX_CLIENTS; i++) {
		client_states[i].xc = xc;
		client_states[i].active = false;
		for (uint32_t j = 0; j < MAX_CLIENT_SWAPCHAINS; j++) {
			client_states[i].swapchain_handles[j] = 0;
			client_states[i].xscs[j] = NULL;
			client_states[i].swapchain_data[j].swapchain_active =
			    false;
			client_states[i].swapchain_data[j].swapchain_created =
			    false;

			client_states[i].render_state.reset_for_session = true;
		}
		client_states[i].num_swapchains = 0;
	}
	int32_t active_client = -1;

	struct xrt_swapchain *idle_left_swapchain = comp_swapchain_create(
	    xc, XRT_SWAPCHAIN_CREATE_STATIC_IMAGE, XRT_SWAPCHAIN_USAGE_SAMPLED,
	    1, VK_FORMAT_R8G8B8A8_UNORM, 1, 1024, 1024, 1, 1, 1);
	struct xrt_swapchain *idle_right_swapchain = comp_swapchain_create(
	    xc, XRT_SWAPCHAIN_CREATE_STATIC_IMAGE, XRT_SWAPCHAIN_USAGE_SAMPLED,
	    1, VK_FORMAT_R8G8B8A8_UNORM, 1, 1024, 1024, 1, 1, 1);

	pthread_t ipc_thread;
	if (pthread_create(&ipc_thread, NULL, *ipc_thread_main,
	                   (void *)(client_states))) {
		printf("ERROR: Cannot create allocator thread!");
		return false;
	}

	uint32_t l_present_index, r_present_index;
	while (1) {

		// service any pending swapchain creation requests
		// TODO: SANITY, LOCKING
		for (uint32_t client_id = 0; client_id < IPC_MAX_CLIENTS;
		     client_id++) {
			ipc_client_state_t *client_state =
			    &client_states[client_id];
			for (uint32_t swapchain_id = 0;
			     swapchain_id < MAX_CLIENT_SWAPCHAINS;
			     swapchain_id++) {
				swapchain_data_t *swapchain_data =
				    &client_states[client_id]
				         .swapchain_data[swapchain_id];
				if (swapchain_data->swapchain_active == true &&
				    swapchain_data->swapchain_created ==
				        false) {
					// create the swapchain
					client_state->xscs[swapchain_id] =
					    xrt_comp_create_swapchain(
					        client_state->xc,
					        XRT_SWAPCHAIN_CREATE_STATIC_IMAGE,
					        XRT_SWAPCHAIN_USAGE_SAMPLED |
					            XRT_SWAPCHAIN_USAGE_COLOR,
					        swapchain_data->num_images,
					        swapchain_data->format, 1,
					        swapchain_data->width,
					        swapchain_data->height, 1, 1,
					        1);
					swapchain_data->swapchain_created =
					    true;
				}
			}

			if (client_state->active) {
				active_client = client_id;
			}
		}


		if (active_client < 0 ||
		    client_states[active_client].num_swapchains == 0) {
			// render our dummy frames
			// printf("rendering idle client\n");
			l_present_index = 0;
			r_present_index = 0;
			struct comp_swapchain *cl =
			    comp_swapchain(idle_left_swapchain);
			struct comp_swapchain *cr =
			    comp_swapchain(idle_right_swapchain);
			comp_renderer_frame(c->r, &cl->images[l_present_index],
			                    0, &cr->images[r_present_index], 0);
		} else {

			// our ipc server thread will fill in l & r swapchain
			// indices and toggle wait to false when the client
			// calls end_frame, signalling us to render.
			render_state_t *render_state =
			    &client_states[active_client].render_state;

			if (render_state->rendering) {

				// if we have had a change of renderer params
				// due to a new session being started, we need
				// to reconfigure our pipeline
				if (render_state->reset_for_session) {
					comp_renderer_reset(c->r);
					render_state->reset_for_session = false;
				}
				// TODO: deal with cases where we have 1
				// swapchain with multiple array layers or more
				// than 2 views

				// TODO: swapchain locking/buffer-rotation
				// printf("rendering active client
				// %d\n",active_client);
				struct comp_swapchain *cl = comp_swapchain(
				    client_states[active_client].xscs[0]);
				struct comp_swapchain *cr = comp_swapchain(
				    client_states[active_client].xscs[1]);

				comp_renderer_frame(
				    c->r,
				    &cl->images[render_state->l_render_index],
				    0,
				    &cr->images[render_state->r_render_index],
				    0);

				render_state->rendering =
				    false; // set our client state back to
				           // waiting.
			}
		}
	}
}


void *
ipc_thread_main(void *_client_states)
{
	// create our domain socket - TODO: implement support for multiple
	// clients - currently we just assume we have a single client.

	// fd_set client_fds[IPC_MAX_CLIENTS]; //TODO: implement select or epoll
	ipc_client_state_t *client_states =
	    (ipc_client_state_t *)_client_states;

	int msg_socket_fd; // used for bidirectional comms
	struct sockaddr_un msg_socket_addr;

	uint8_t recv_ipc_message[IPC_BUF_SIZE];

	ssize_t len;
	socklen_t msg_socket_addr_len = sizeof(msg_socket_addr);

	bool msg_socket_created = true;
	bool msg_socket_bound = true;


	if ((msg_socket_fd = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
		printf("Message Socket Create Error!\n");
		msg_socket_created = false;
	}

	if (msg_socket_created) {
		memset(&msg_socket_addr, 0, sizeof(msg_socket_addr));
		msg_socket_addr.sun_family = AF_UNIX;
		strcpy(msg_socket_addr.sun_path, MSG_SOCK_FILE);
		unlink(MSG_SOCK_FILE);

		if (bind(msg_socket_fd, (struct sockaddr *)&msg_socket_addr,
		         msg_socket_addr_len) < 0) {
			printf("MSG Socket Bind Error!\n");
			msg_socket_bound = false;
		}

		if (listen(msg_socket_fd, IPC_MAX_CLIENTS) < 0) {
			printf("MSG Listen error!\n");
			msg_socket_bound = false;
		}
	}


	if (msg_socket_bound) {
		// run in an endless loop servicing connections
		printf("Looping waiting for connections.\n");
		while (1) {
			uint32_t client_index = 0; // TODO: loop over multiple
			                           // clients/use select/epoll
			ipc_client_state_t *client_state =
			    &client_states[client_index];

			if ((client_state->ipc_socket_fd =
			         accept(msg_socket_fd, NULL, NULL)) == -1) {
				printf("Accept error!\n");
				exit(0);
			}

			if (client_state->ipc_socket_fd > 0) {
				printf("SERVER: got client connection!\n");
			}

			int32_t ret = 0;
			while (
			    (len = recvfrom(client_state->ipc_socket_fd,
			                    &recv_ipc_message, IPC_BUF_SIZE, 0,
			                    (struct sockaddr *)&msg_socket_addr,
			                    &msg_socket_addr_len)) > 0) {
				// check the first 4 bytes of the message and
				// dispatch accordingly:
				ipc_command_t ipc_command =
				    *(uint32_t *)recv_ipc_message;
				switch (ipc_command) {
				case IPC_BEGIN_SESSION:
				case IPC_END_SESSION:
					handle_session_message(
					    client_state,
					    (ipc_session_message_t *)
					        recv_ipc_message);
					break;
				case IPC_SWAPCHAIN_CREATE:
				case IPC_SWAPCHAIN_ACQUIRE_FDS:
				case IPC_SWAPCHAIN_DESTROY:
					handle_swapchain_message(
					    client_state,
					    (ipc_swapchain_message_t *)
					        recv_ipc_message);
					break;
				case IPC_COMPOSITOR_BEGIN_FRAME:
				case IPC_COMPOSITOR_WAIT_FRAME:
				case IPC_COMPOSITOR_DISCARD_FRAME:
				case IPC_COMPOSITOR_END_FRAME:
					handle_compositor_frame_message(
					    client_state,
					    (ipc_compositor_frame_message_t *)
					        recv_ipc_message);
					break;
				case IPC_SWAPCHAIN_ACQUIRE_IMAGE:
				case IPC_SWAPCHAIN_WAIT_IMAGE:
				case IPC_SWAPCHAIN_RELEASE_IMAGE:
					handle_swapchain_image_message(
					    client_state,
					    (ipc_swapchain_image_message_t *)
					        recv_ipc_message);
					break;
				default:
					printf("UNHANDLED IPC MESSAGE! %d\n",
					       ipc_command);
					exit(0);
					break;
				}
			}
		} // while 1
	}

	if (msg_socket_fd >= 0) {
		close(msg_socket_fd);
	}

	return NULL;
}


void
handle_session_message(ipc_client_state_t *c, ipc_session_message_t *message)
{
	ipc_session_message_t response;
	memset(&response, 0, sizeof(ipc_session_message_t));
	response.command = message->command;
	response.result = IPC_FAILURE;
	switch (message->command) {
	case IPC_BEGIN_SESSION:
		c->render_state.reset_for_session = true;
		c->active = true;
		response.result = IPC_SUCCESS;
		break;
	case IPC_END_SESSION:
		c->active = false;
		response.result = IPC_SUCCESS;
		break;
	default:
		printf(
		    "Unhandled session command. We should never get here!\n");
		exit(0);
	}
	server_send_message(c->ipc_socket_fd, (uint8_t *)&response,
	                    sizeof(ipc_session_message_t));
}

void
handle_compositor_frame_message(ipc_client_state_t *c,
                                ipc_compositor_frame_message_t *message)
{
	ipc_compositor_frame_message_t response;
	memset(&response, 0, sizeof(ipc_compositor_frame_message_t));
	response.command = message->command;
	response.result = IPC_FAILURE;
	switch (message->command) {
	case IPC_COMPOSITOR_BEGIN_FRAME:
		printf("SERVER: sending compositor begin frame response\n");
		response.result = IPC_SUCCESS;
		break;
	case IPC_COMPOSITOR_WAIT_FRAME:
		printf("SERVER: sending compositor wait frame response\n");
		response.result = IPC_SUCCESS;
		break;
	case IPC_COMPOSITOR_DISCARD_FRAME:
		printf("SERVER: sending compositor discard frame response\n");
		response.result = IPC_SUCCESS;
		break;
	case IPC_COMPOSITOR_END_FRAME:
		printf("SERVER: sending compositor end frame response\n");
		c->render_state.rendering = true;
		c->render_state.l_render_index =
		    0; // TODO: this is hardcoded for now
		c->render_state.r_render_index =
		    0; // TODO: this is hardcoded for now
		response.result = IPC_SUCCESS;
		break;
	default:
		printf(
		    "Unhandled swapchain image command. We should never get "
		    "here!\n");
		exit(0);
	}
	server_send_message(c->ipc_socket_fd, (uint8_t *)&response,
	                    sizeof(ipc_compositor_frame_message_t));
}

void
handle_swapchain_message(ipc_client_state_t *c,
                         ipc_swapchain_message_t *message)
{
	ipc_swapchain_message_t response;
	memset(&response, 0, sizeof(ipc_swapchain_message_t));
	response.command = message->command;
	response.result = IPC_FAILURE;
	switch (message->command) {
	case IPC_SWAPCHAIN_CREATE: {
		uint32_t swapchain_index = c->num_swapchains;
		// this needs to be constructed on the main thread, so we fill
		// in the info, and wait until our main thread sets created to
		// true.
		// TODO: SANITY, LOCKING
		c->swapchain_data[swapchain_index].width =
		    message->fd_swapchain.width;
		c->swapchain_data[swapchain_index].height =
		    message->fd_swapchain.height;
		c->swapchain_data[swapchain_index].format =
		    message->fd_swapchain.format;
		c->swapchain_data[swapchain_index].num_images =
		    message->fd_swapchain.num_fds;

		c->swapchain_data[swapchain_index].swapchain_created = false;
		c->swapchain_data[swapchain_index].swapchain_active = true;

		while (!c->swapchain_data[swapchain_index].swapchain_created) {
			// block - do nothing;
		}

		c->swapchain_handles[swapchain_index] =
		    swapchain_index; // our handle is just the index for now
		c->num_swapchains++;

		// return our result to the caller.
		struct comp_swapchain *cs =
		    comp_swapchain(c->xscs[swapchain_index]);
		response.result = IPC_SUCCESS; // success
		response.fd_swapchain.width = cs->base.base.width;
		response.fd_swapchain.height = cs->base.base.height;
		response.fd_swapchain.format = cs->base.base.format;
		response.fd_swapchain.size =
		    cs->base.images
		        ->size; // assume all images are the same size
		response.fd_swapchain.num_fds =
		    c->xscs[swapchain_index]->num_images;

		for (uint32_t i = 0; i < c->xscs[swapchain_index]->num_images;
		     i++) {
			response.fd_swapchain.swapchain_fds[i] =
			    cs->base.images[i].fd;
		}

		response.fd_swapchain.swapchain_handle =
		    c->swapchain_handles[swapchain_index];
		printf("SERVER: sending swapchain create response\n");
		server_send_message(c->ipc_socket_fd, (uint8_t *)&response,
		                    sizeof(ipc_swapchain_message_t));
	} break;
	case IPC_SWAPCHAIN_ACQUIRE_FDS: {
		uint32_t swapchain_index =
		    message->fd_swapchain.swapchain_handle;
		struct comp_swapchain *cs =
		    comp_swapchain(c->xscs[swapchain_index]);
		response.result = IPC_SUCCESS;
		response.fd_swapchain.width = cs->base.base.width;
		response.fd_swapchain.height = cs->base.base.height;
		response.fd_swapchain.format = cs->base.base.format;
		response.fd_swapchain.size =
		    cs->base.images[0]
		        .size; // assume all images are the same size
		response.fd_swapchain.num_fds =
		    c->xscs[swapchain_index]->num_images;
		for (uint32_t i = 0; i < c->xscs[swapchain_index]->num_images;
		     i++) {
			response.fd_swapchain.swapchain_fds[i] =
			    cs->base.images[i].fd;
		}
		response.fd_swapchain.swapchain_handle =
		    c->swapchain_handles[swapchain_index];
		printf("SERVER: sending swapchain acquire fds response\n");
		server_send_swapchain_with_fds(c->ipc_socket_fd, &response);
	} break;
	case IPC_SWAPCHAIN_DESTROY:
		printf("SERVER: sending swapchain destroy response\n");
		response.result = IPC_SUCCESS;
		server_send_message(c->ipc_socket_fd, (uint8_t *)&response,
		                    sizeof(ipc_swapchain_message_t));
		break;
	default:
		printf(
		    "Unhandled swapchain command. We should never get here!\n");
		exit(0);
	}
}

void
handle_swapchain_image_message(ipc_client_state_t *c,
                               ipc_swapchain_image_message_t *message)
{
	ipc_swapchain_image_message_t response;
	memset(&response, 0, sizeof(ipc_swapchain_image_message_t));
	response.command = message->command;
	response.result = IPC_FAILURE;
	switch (message->command) {
	case IPC_SWAPCHAIN_ACQUIRE_IMAGE:
		printf("SERVER: sending swapchain acquire image response\n");
		response.result = IPC_SUCCESS;
		break;
	case IPC_SWAPCHAIN_WAIT_IMAGE:
		printf("SERVER: sending swapchain wait image response\n");
		response.result = IPC_SUCCESS;
		break;
	case IPC_SWAPCHAIN_RELEASE_IMAGE:
		printf("SERVER: sending swapchain release image response\n");
		response.result = IPC_SUCCESS;
		break;
	default:
		printf(
		    "Unhandled swapchain image command. We should never get "
		    "here!\n");
		exit(0);
	}
	server_send_message(c->ipc_socket_fd, (uint8_t *)&response,
	                    sizeof(ipc_session_message_t));
}
