#include <pthread.h>
#include <stdlib.h>
#include "ipc/ipc_server.h"

#define MAX_CLIENT_SWAPCHAINS 8
#define IPC_MAX_CLIENTS 8

typedef struct swapchain_data
{
	uint32_t width;
	uint32_t height;
	uint64_t format;
	uint32_t num_images;
	bool swapchain_created;
	bool swapchain_active;
} swapchain_data_t;

typedef struct render_state
{
	bool reset_for_session;
	bool rendering;
	uint32_t l_render_index;
	uint32_t r_render_index;
} render_state_t;

typedef struct ipc_client_state
{
	struct xrt_compositor *xc;
	uint32_t num_swapchains; // number of swapchains in use by client
	uint32_t
	    swapchain_handles[MAX_CLIENT_SWAPCHAINS]; // handles for dealing
	                                              // with swapchains via ipc
	struct xrt_swapchain
	    *xscs[MAX_CLIENT_SWAPCHAINS]; // ptrs to the swapchains
	swapchain_data_t swapchain_data[MAX_CLIENT_SWAPCHAINS];
	int ipc_socket_fd;     // socket fd used for client comms
	bool is_authenticated; // not implemented
	uint32_t client_id;    // not implemented
	render_state_t render_state;
	bool active;
} ipc_client_state_t;

void
handle_session_message(ipc_client_state_t *client_state,
                       ipc_session_message_t *message);
void
handle_compositor_frame_message(ipc_client_state_t *client_state,
                                ipc_compositor_frame_message_t *message);
void
handle_swapchain_message(ipc_client_state_t *client_state,
                         ipc_swapchain_message_t *message);
void
handle_swapchain_image_message(ipc_client_state_t *client_state,
                               ipc_swapchain_image_message_t *message);
