
#include <string.h>
#include <stdio.h>
#include "ipc/ipc_client.h"
#include "xrt/xrt_compositor.h"
#include "util/u_misc.h"


bool ipc_connect(ipc_connection_t* ipc_c)
{
    struct sockaddr_un addr;

    ipc_c->socket_created = true;
    ipc_c->socket_connected = true;

    ipc_c->print_spew = true; // TODO: hardcoded - fetch from settings
    ipc_c->print_debug = true; // TODO: hardcoded - fetch from settings


    //create our IPC socket

    if ((ipc_c->socket_fd = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
        IPC_DEBUG(ipc_c,"Socket Create Error!\n");
        ipc_c->socket_created=false;
        exit(0);
    }

    if ( ipc_c->socket_created) {
        memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, MSG_SOCK_FILE);
        if (connect(ipc_c->socket_fd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
        {
            IPC_DEBUG(ipc_c,"Socket Connect error!\n");
            ipc_c->socket_connected = false;
            exit(0);
        }
    }

    if (ipc_c->socket_connected) {
        return true;
    }
    return false;
}

 // since our protocol is set up such that our message response is always
 // of the same type as the sent message, the response overwrites the sent struct in-place

 ipc_result_t ipc_send_message(ipc_connection_t* ipc_c, uint8_t* message,uint32_t size) {
     if (ipc_c->socket_connected) {
        if (send(ipc_c->socket_fd, message, size, 0) == -1)
             {
                 IPC_DEBUG(ipc_c,"Error sending - cannot continue!\n");
                 //TODO: exit here?
                 exit(0);
                 return IPC_FAILURE;
             }
             else
             {
                //wait for the response
                struct iovec iov;
                struct msghdr msg;
                iov.iov_base = message;
                iov.iov_len = size;

                memset(&msg,0,sizeof(struct msghdr));
                msg.msg_name = 0;
                msg.msg_namelen = 0;
                msg.msg_iov = &iov;
                msg.msg_iovlen = 1;
                msg.msg_flags = 0;

                size_t len = recvmsg(ipc_c->socket_fd, &msg,0);

                if (len < 0) {
                    IPC_DEBUG(ipc_c,"recvmsg failed with error: %s\n", strerror(errno));
                    exit(0);
                    return IPC_FAILURE;
                }

            if (len == 0) {
                IPC_DEBUG(ipc_c,"recvmsg failed with error: no data\n");
                exit(0);
                return IPC_FAILURE;
            }

            return IPC_SUCCESS;
        }
     }

 }

ipc_result_t ipc_acquire_swapchain_fds(ipc_connection_t* ipc_c, ipc_swapchain_message_t* message, int* out_fds) {

    if (send(ipc_c->socket_fd, message, sizeof(ipc_swapchain_message_t), 0) == -1){
         IPC_DEBUG(ipc_c,"Error sending - cannot continue!\n");
         return IPC_FAILURE;
     }

     struct msghdr msg;
     struct cmsghdr *cmsg;
     struct iovec iov;
     char buf[CMSG_SPACE(message->fd_swapchain.num_fds * sizeof(int))];
     memset(buf, 0, sizeof(buf));
     iov.iov_base = message;
     iov.iov_len = sizeof(ipc_swapchain_message_t);

     memset (&msg,0,sizeof (struct msghdr));
     msg.msg_iov = &iov;
     msg.msg_iovlen = 1;
     msg.msg_control = buf;
     msg.msg_controllen = sizeof(buf);

     ssize_t len = recvmsg (ipc_c->socket_fd, &msg, 0);

    if (len < 0) {
        IPC_DEBUG(ipc_c,"recvmsg failed with error: %s\n", strerror(errno));
        return -1;
    }

    if (len == 0) {
        IPC_DEBUG(ipc_c,"recvmsg failed with error: no data\n");
        return -1;
    }

    IPC_SPEW(ipc_c,"Image size: %d x %d\n",message->fd_swapchain.width,message->fd_swapchain.height);

    cmsg = CMSG_FIRSTHDR(&msg);
    memcpy(out_fds,  (int *) CMSG_DATA(cmsg), sizeof(int) * message->fd_swapchain.num_fds);

    return IPC_SUCCESS;
 }

void compositor_disconnect(ipc_connection_t* ipc_c)
{
    if (ipc_c->socket_connected) {
        close(ipc_c->socket_fd);
    }
}


struct xrt_swapchain *
ipc_compositor_swapchain_create(struct xrt_compositor *xc,
                      enum xrt_swapchain_create_flags create,
                      enum xrt_swapchain_usage_bits bits,
                      uint32_t num_images,
                      int64_t format,
                      uint32_t sample_count,
                      uint32_t width,
                      uint32_t height,
                      uint32_t face_count,
                      uint32_t array_size,
                      uint32_t mip_count) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor swapchain create\n");

    int remote_fds[MAX_SWAPCHAIN_FDS];
    ipc_fd_swapchain_t remote_swapchain;
    ipc_swapchain_message_t swapchain_msg;
    swapchain_msg.command=IPC_SWAPCHAIN_CREATE;
    swapchain_msg.result=IPC_FAILURE; //initialise result
    swapchain_msg.fd_swapchain.width= width;
    swapchain_msg.fd_swapchain.height= height;
    swapchain_msg.fd_swapchain.format= format;
    swapchain_msg.fd_swapchain.num_fds = num_images;
    ipc_result_t r = ipc_send_message(xc->ipc_c,(uint8_t*)(&swapchain_msg),sizeof(ipc_swapchain_message_t));
    IPC_SPEW(xc->ipc_c,"got response from compositor IPC_SWAPCHAIN_CREATE! %d %d\n",r,swapchain_msg.fd_swapchain.swapchain_handle);

    if (r == IPC_SUCCESS) {
        //NOTE: swapchain_handle is already filled in from our swapchain create response
        swapchain_msg.command=IPC_SWAPCHAIN_ACQUIRE_FDS;
        swapchain_msg.result=IPC_FAILURE; //initialise result
        remote_swapchain = swapchain_msg.fd_swapchain;
        IPC_SPEW(xc->ipc_c,"IPC: acquire swapchain\n");
        ipc_result_t r = ipc_acquire_swapchain_fds(xc->ipc_c,&swapchain_msg,remote_fds);
        IPC_SPEW(xc->ipc_c,"got response from compositor IPC_ACQUIRE_SWAPCHAIN_FDS! %d %d\n",r,remote_swapchain.swapchain_handle);

        for (uint32_t i =0;i<remote_swapchain.num_fds;i++) {
            IPC_SPEW(xc->ipc_c,"server fd: %d client fd:%d\n",remote_swapchain.swapchain_fds[i],remote_fds[i]);
        }
        struct xrt_swapchain_fd *xscfd = U_TYPED_CALLOC(struct xrt_swapchain_fd);
        xscfd->base.width = swapchain_msg.fd_swapchain.width;
        xscfd->base.height = swapchain_msg.fd_swapchain.height;
        xscfd->base.format = swapchain_msg.fd_swapchain.format;
        xscfd->base.array_size =1;
        xscfd->base.num_images = remote_swapchain.num_fds;
        xscfd->base.wait_image = ipc_compositor_swapchain_wait_image;
        xscfd->base.acquire_image= ipc_compositor_swapchain_acquire_image;
        xscfd->base.release_image = ipc_compositor_swapchain_release_image;
        xscfd->base.destroy  = ipc_compositor_swapchain_destroy;
        xscfd->base.ipc_c = xc->ipc_c;

        for (uint32_t i=0;i< num_images;i++) {
            xscfd->images[i].fd = remote_fds[i];
            xscfd->images[i].size = swapchain_msg.fd_swapchain.size;
        }
        return (struct xrt_swapchain*)(xscfd);
    }
    return NULL;
}



void ipc_compositor_begin_session(struct xrt_compositor *xc,
                                  enum xrt_view_type view_type) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor begin session\n");
    ipc_session_message_t message;
    message.command = IPC_BEGIN_SESSION;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xc->ipc_c,&message,sizeof (ipc_session_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xc->ipc_c,"IPC: Error sending begin session!\n");
    }
}

void ipc_compositor_end_session(struct xrt_compositor *xc) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor end session\n");
    ipc_session_message_t message;
    message.command = IPC_END_SESSION;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xc->ipc_c,&message,sizeof (ipc_session_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xc->ipc_c,"IPC: Error sending end session!\n");
    }
}

void ipc_compositor_wait_frame(struct xrt_compositor *xc,
                   uint64_t *predicted_display_time,
                               uint64_t *predicted_display_period) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor wait frame\n");
    ipc_compositor_frame_message_t message;
    message.command = IPC_COMPOSITOR_WAIT_FRAME;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xc->ipc_c,&message,sizeof (ipc_compositor_frame_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xc->ipc_c,"IPC: Error sending wait frame!\n");
    }

}

void ipc_compositor_begin_frame(struct xrt_compositor *xc) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor begin frame\n");
    ipc_compositor_frame_message_t message;
    message.command = IPC_COMPOSITOR_BEGIN_FRAME;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xc->ipc_c,&message,sizeof (ipc_compositor_frame_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xc->ipc_c,"IPC: Error sending begin frame!\n");
    }
}

void ipc_compositor_end_frame(struct xrt_compositor *xc,
                  enum xrt_blend_mode blend_mode,
                  struct xrt_swapchain **xscs,
                  const uint32_t *image_index,
                  uint32_t *layers,
                              uint32_t num_swapchains) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor end frame\n");
    ipc_compositor_frame_message_t message;
    message.command = IPC_COMPOSITOR_END_FRAME;
    message.result=IPC_FAILURE;
    message.compositor_frame.blend_mode = blend_mode;
    ipc_result_t r = ipc_send_message(xc->ipc_c,(uint8_t*)&message,sizeof (ipc_compositor_frame_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xc->ipc_c,"IPC: Error sending end frame!\n");
    }
}


void ipc_compositor_discard_frame(struct xrt_compositor *xc) {
    IPC_SPEW(xc->ipc_c,"IPC: compositor discard frame\n");
    ipc_compositor_frame_message_t message;
    message.command = IPC_COMPOSITOR_DISCARD_FRAME;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xc->ipc_c,(uint8_t*)&message,sizeof (ipc_compositor_frame_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xc->ipc_c,"IPC: Error sending discard frame!\n");
    }
}

void ipc_compositor_destroy(struct xrt_compositor *xc) {
    IPC_SPEW(xc->ipc_c,"IPC:  NOT IMPLEMENTED compositor destroy\n");
}

void ipc_compositor_swapchain_destroy(struct xrt_compositor *xc) {
    IPC_SPEW(xc->ipc_c,"IPC: NOT IMPLEMENTED compositor swapchain destroy\n");
}

bool ipc_compositor_swapchain_wait_image(struct xrt_swapchain *xsc,
                              uint64_t timeout,
                                         uint32_t index) {
    IPC_SPEW(xsc->ipc_c,"IPC: compositor swapchain wait image\n");
    ipc_swapchain_image_message_t message;
    message.command = IPC_SWAPCHAIN_WAIT_IMAGE;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xsc->ipc_c,(uint8_t*)&message,sizeof (ipc_swapchain_image_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xsc->ipc_c,"IPC: Error sending swapchain wait image!\n");
    }
    return true;
}
bool ipc_compositor_swapchain_acquire_image(struct xrt_swapchain *xsc, uint32_t *index) {
    IPC_SPEW(xsc->ipc_c,"IPC: compositor swapchain acquire image\n");
    ipc_swapchain_image_message_t message;
    message.command = IPC_SWAPCHAIN_ACQUIRE_IMAGE;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xsc->ipc_c,(uint8_t*)&message,sizeof (ipc_swapchain_image_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xsc->ipc_c,"IPC: Error sending swapchain acquire image!\n");
    }
    return true;
}
bool ipc_compositor_swapchain_release_image(struct xrt_swapchain *xsc, uint32_t index) {
    IPC_SPEW(xsc->ipc_c,"IPC: compositor swapchain release image\n");
    ipc_swapchain_image_message_t message;
    message.command = IPC_SWAPCHAIN_RELEASE_IMAGE;
    message.result=IPC_FAILURE;
    ipc_result_t r = ipc_send_message(xsc->ipc_c,(uint8_t*)&message,sizeof (ipc_swapchain_image_message_t));
    if (r != IPC_SUCCESS) {
        IPC_DEBUG(xsc->ipc_c,"IPC: Error sending swapchain release image!\n");
    }
    return true;
}



