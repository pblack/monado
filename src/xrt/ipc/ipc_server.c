
#include "ipc/ipc_server.h"

int server_send_message(int socket, uint8_t* ipc_message, uint32_t len) {

    struct msghdr msg;
    struct iovec iov;
    iov.iov_base = ipc_message;
    iov.iov_len = len;

    memset(&msg,0,sizeof(struct msghdr));
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_flags = 0;
    ssize_t ret = sendmsg(socket, &msg, 0);

    if (ret == -1) {
        printf("sending plain message on socket %d failed with error: %s\n", socket, strerror(errno));
    }

    return ret;
}

int server_send_swapchain_with_fds(int socket, ipc_swapchain_message_t* ipc_swapchain_message)
{
    struct msghdr msg;
    struct iovec iov;

    uint32_t num_fds = ipc_swapchain_message->fd_swapchain.num_fds;
    char cmsgbuf[CMSG_SPACE(sizeof(int) * num_fds )];

    iov.iov_base = ipc_swapchain_message;
    iov.iov_len = sizeof(ipc_swapchain_message_t);

    memset(&msg,0,sizeof(struct msghdr));
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_flags = 0;
    msg.msg_control = cmsgbuf;
    msg.msg_controllen = CMSG_LEN(sizeof(int) * num_fds);

    struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int)* num_fds);

    memcpy ((int *)CMSG_DATA(cmsg), ipc_swapchain_message->fd_swapchain.swapchain_fds, num_fds * sizeof (int));

    ssize_t ret = sendmsg(socket, &msg, 0);

    if (ret == -1) {
        printf("sending %d FDs on socket %d failed with error: %s\n", num_fds,socket, strerror(errno));
    }

    return ret;
}
