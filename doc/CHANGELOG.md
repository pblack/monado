# Changelog for Monado

```txt
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2020 Collabora, Ltd. and the Monado contributors
```

## Monado 0.1.0 (2020-02-24)

Initial (non-release) tag to promote/support packaging.
